﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace lab1oop
{
    class Program
    {
        static void Main(string[] args)
        {
            Patient[] patients = GetPatientsFromFile();

            while (true)
            {
                PrintMainMenu();
                int userAnswer = Convert.ToInt32(Console.ReadLine());
                if (userAnswer == 3)
                    break;
                RunAction(userAnswer, patients);
            }
        }

        public static string[] GetFileContent(string fileName)
        {
            StreamReader reader = new StreamReader(fileName);
            string[] content = reader.ReadToEnd().Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
            reader.Close();
            return content;
        }
        public static Patient[] GetPatientsFromStrings(string[] content)
        {
            Patient[] patients = new Patient[content.Length];
            for (int i = 0; i < patients.Length; i++)
                patients[i] = new Patient(content[i]);
            return patients;
        }
        public static Patient[] GetPatientsFromFile()
        {
            Console.Write("Введите имя файла > ");
            string fileName = Console.ReadLine();
            return GetPatientsFromStrings(GetFileContent(fileName));
        }

        public static void PrintMainMenu()
        {
            Console.Clear();
            Console.WriteLine("Выберите действие:");
            Console.WriteLine("1. Вывести пациентов по заданному диагнозу");
            Console.WriteLine("2. Записать в файл информацию о пациентах");
            Console.WriteLine("3. Выход");
            Console.Write("> ");
        }
        public static void RunAction(int actionIndex, Patient[] patients)
        {
            if (actionIndex == 1)
                RunPatientsByDiagnosAction(patients);
            else
                RunPatientsToFileAction(patients);
        }
        public static void RunPatientsByDiagnosAction(Patient[] patients)
        {
            PrintSelectDiagnosMenu();
            int diagnosIndex = Convert.ToInt32(Console.ReadLine()) - 1;
            Diagnosis diagnosis = (Diagnosis)Enum.GetValues(typeof(Diagnosis)).GetValue(diagnosIndex);
            PrintFormatMenu();
            ConsoleKeyInfo pressedKey = Console.ReadKey(true);
            if (pressedKey.Key == ConsoleKey.Home)
                PrintHomeModeRecords(diagnosis, patients);
            else if (pressedKey.Key == ConsoleKey.End)
                PrintEndModeRecords(diagnosis, patients);
            Console.ReadKey();
        }
        public static void PrintSelectDiagnosMenu()
        {
            string[]diagnosis = Enum.GetNames(typeof(Diagnosis));
            Console.Clear();
            Console.WriteLine("Выберите диагноз");
            for (int i = 0; i < diagnosis.Length; i++)
                Console.WriteLine((i + 1) + ". " + diagnosis[i]);
            Console.Write("> ");
        }
        public static void PrintFormatMenu()
        {
            Console.Clear();
            Console.WriteLine("Укажите формат вывода");
            Console.WriteLine("Home - Фамилия и возраст");
            Console.WriteLine("End  - Фамилия, дата поступления и год рождения");
        }
        public static void PrintHomeModeRecords(Diagnosis diagnosis, Patient[] patients)
        {
            Console.Clear();
            string[] fieldNames = { "Имя", "Возраст" };
            Table table = new Table(diagnosis.ToString(), fieldNames);
            Console.Write(table.Head());
            for (int i = 0; i < patients.Length; i++)
                if (patients[i].Diagnosis == diagnosis)
                    Console.Write(table.Row(new string[] { patients[i].Name, patients[i].Age().ToString() }));
            Console.Write(table.End());
        }
        public static void PrintEndModeRecords(Diagnosis diagnosis, Patient[] patients)
        {
            Console.Clear();
            string[] fieldNames = { "Имя", "Год рождения", "Дата поступления" };
            Table table = new Table(diagnosis.ToString(), fieldNames);
            Console.Write(table.Head());
            for (int i = 0; i < patients.Length; i++)
                if (patients[i].Diagnosis == diagnosis)
                     Console.Write(table.Row(new string[] { patients[i].Name, patients[i].ReceiptDate.ToShortDateString(), patients[i].BirthYear.ToString() }));
            Console.Write(table.End());
        }

        public static void RunPatientsToFileAction(Patient[] patients)
        {
            StreamWriter writer = new StreamWriter(GetFileName());
            string[] diagnosisNames = Enum.GetNames(typeof(Diagnosis));
            Array.Sort(patients);
            for (int i = 0; i < diagnosisNames.Length; i++)
            {
                bool isFind = false;
                string[] fields = { "Фамилия", 
                                    "Пребывание",
                                    "Возраст" };
                Table table = new Table(diagnosisNames[i], fields);

                for (int j = 0; j < patients.Length; j++)
                    if (patients[j].Diagnosis.ToString() == diagnosisNames[i])
                    {
                        if (!isFind)
                        {
                            isFind = true;
                            writer.Write(table.Head());
                        }
                        string[] fieldsValues = {
                            patients[j].Name,
                            patients[j].StayLengthToDays.ToString(),
                            patients[j].Age().ToString() 
                        };
                        writer.Write(table.Row(fieldsValues));
                    }
                if (isFind)
                    writer.Write(table.End());
                else
                    writer.WriteLine("Больные не обнаружены");
                writer.WriteLine();

            }
            writer.Close();
        }
        public static string GetFileName()
        {
            Console.Clear();
            Console.Write("Введите имя файла > ");
            return Console.ReadLine();
        }
    }
}
