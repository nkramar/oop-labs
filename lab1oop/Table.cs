﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace lab1oop
{
    public class Table
    {
        string[] fields;
        string name;
        int fieldLength = 20;
        int numFieldLength = 3;
        int currentRowIndex;

        public string[] Fields
        {
            get { return fields; }
            set { fields = value; }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public Table(string name, string[] fields)
        {
            this.name = name;
            this.fields = fields;
        }

        string Line(char[] elements)
        {
            char[] result = new char[2 + numFieldLength + (fieldLength + 1) * fields.Length];
            result[0] = elements[0];
            int currentCharNumber = 1;
            for (int i = 0; i < fields.Length + 1; i++)
            {
                int currentFieldLength = i == 0 ? numFieldLength : fieldLength;
                for (int j = 0; j < currentFieldLength; j++)
                    result[currentCharNumber++] = elements[1];
                if (i == fields.Length)
                    result[currentCharNumber++] = elements[3];
                else
                    result[currentCharNumber++] = elements[2];
            }
            return (new string(result)) + "\n";
        }
        string Row(string numFieldValue, string[] fieldsValues)
        {
            int currentChar = 0;
            char[] result = new char[2 + numFieldLength + (fieldLength + 1) * fields.Length];
            result[currentChar++] = '║';
            for (int i = 0; i < fieldsValues.Length + 1; i++)
            {
                int currentFieldLength = i == 0 ? numFieldLength : fieldLength;
                string currentFieldValue = i == 0 ? numFieldValue : fieldsValues[i - 1];
                for (int j = 0; j < currentFieldLength - currentFieldValue.Length; j++)
                    result[currentChar++] = ' ';
                for (int j = 0; j < currentFieldValue.Length; j++)
                    result[currentChar++] = currentFieldValue[j];
                result[currentChar++] = '║';
            }
            return (new string(result)) + "\n";
        }

        public string Head()
        {
            currentRowIndex = 1;
            string result = "Table " + name + "\n";
            result += Line("╔═╦╗".ToCharArray());
            result += Row("№", fields);
            result += Line("╠═╬╣".ToCharArray());
            return result;
        }
        public string Row(string[] values)
        {
            return Row((currentRowIndex++).ToString(), values);
        }
        public string End()
        {
            return Line("╚═╩╝".ToCharArray());
        }
    }
}
