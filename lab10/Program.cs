﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Collections;
using System.IO;

namespace lab10
{
    class Program
    {
        // массив подписчиков
        static ArrayList Podpischiki = new ArrayList();

        static void Main(string[] args)
        {

            Console.WindowHeight = 50;


            //считываем из файла информацию о всех подписчиках
            LoadFromFile("Подписчики.txt");

            // Определяем обработчика нажатия кнопки Escape
            DelegateClass.ClickButtonEvent += OnEscape;




            while (true)    //бесконечный цикл
            {
                TotalVivod();   // сначала выводим всех подписчиков
                Console.WriteLine("Home - увеличить стоимость подписки на 15%");
                Console.WriteLine("End - уменьшить стоимость подписки на 12%");
                Console.WriteLine("Insert - добавить \"Сушку\" лицам до 14 лет");
                Console.WriteLine("Esc - Выход из программы");

                // ожидаем нажание кнопки и вызываем это событие
                DelegateClass.OnPressKey(Console.ReadKey(true).Key);    

            }

        }


        /// <summary>
        /// Загрузка данных о подписчиках из файла
        /// </summary>
        /// <param name="FileName">имя файла</param>
        static void LoadFromFile(string FileName)
        {
            string[] AllLines = File.ReadAllLines(FileName, Encoding.GetEncoding(1251));

            // берём каждую строку из файла, а точнее уже из нашего ранее прочитанного массива
            foreach (string str in AllLines)
            {
                Podpischik p;
                try
                {
                    p = new Podpischik(str);
                }
                catch
                {
                    break;      //Если при загрузке очередного подписчика возникнет ошибка, то просто пропустим его
                }

                Podpischiki.Add(p); // если удачно создан подписчик, то добавляем его в массив
            }

            // определяем какого размера должен быть массив делегатов.
            // он равен количеству всех подписчиков умноженных на 3 (количество методов у каждого подписчика)
            // а также +1 метод для обработки метода Escape
            DelegateClass.N = 3 * Podpischiki.Count + 1;

            // добавляем каждому подписчику слежение за событиями 3-х кнопок
            for (int i = 0; i < Podpischiki.Count; i++)
            {
                DelegateClass.ClickButtonEvent += (Podpischiki[i] as Podpischik).OnEnd;
                DelegateClass.ClickButtonEvent += (Podpischiki[i] as Podpischik).OnHome;
                DelegateClass.ClickButtonEvent += (Podpischiki[i] as Podpischik).OnInsert;
            }


        }

        /// <summary>
        /// Выводит таблицу всех подписчиков
        /// </summary>
        static void TotalVivod()
        {
            // выводим шапку
            Console.WriteLine("--------------------------------------------------------------------------");
            Console.WriteLine("|  № |   Фамилия И.О  | Возраст|            Издания          | Стоимость |");
            Console.WriteLine("--------------------------------------------------------------------------");

            int i = 1;
            foreach (Podpischik p in Podpischiki)
            {
                Console.WriteLine(p.StringForVivod(i)); //выводим очередного подписчика
                Console.WriteLine("--------------------------------------------------------------------------");
                i++;
            }
        }

         /// <summary>
         /// отбаботчик события нажатия кнопки Escape
         /// </summary>
        static void OnEscape()
        { 
            // завершаем работу программы
            System.Threading.Thread.CurrentThread.Abort(); 
        }
    }
}
