﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;

namespace lab10
{
    class Podpischik
    {
        public string FIO;
        public int Vozrast;

        /// <summary>
        /// Поле массивов Название журналов
        /// </summary>
        public ArrayList NamePress = new ArrayList();

        /// <summary>
        /// Поле массивов Стоимость журналов
        /// </summary>
        public ArrayList CostPress = new ArrayList();

        // индексатор
        public string this[int i]
        {
            get
            {
                return (string)NamePress[i];
            }
            set
            {
                NamePress[i] = value;
            }
        }

        /// <summary>
        /// Общая стоимость подписки
        /// </summary>
        public int TotalCost
        {
            get
            {
                int Total = 0;

                // суммируем все стоимости журналов
                foreach (int c in CostPress)
                {
                    Total += c;
                }
                return Total;
            }
        }


        /// <summary>
        /// Конструктор, который создаёт подписчика по одной только строке из файла
        /// Т.е. разбивает её на составляющие и определяет все поля
        /// </summary>
        /// <param name="stroka">Строка из текстового файла</param>
        public Podpischik(string stroka)
        {
            string[] mas = stroka.Split(new char[] { ';', ',' });  //разбиваем строку на части

            FIO = mas[0];  // на первой позиции стоит ФИО
            Vozrast = int.Parse(mas[1]);    // вторая позиция - возраст

            int i = 2; // переменная цикла, сразу устанавливается в позицию названия первого журнала
            int count = (mas.Length - 2)/2;  // определяем сколько журналов у этого подписчика

            // сначала заполняем массив названий.
            for (int k = 0; k < count; k++)
            {
                NamePress.Add(mas[i]);  //добавляем название очередного журнала
                i++;    // переходим к следующему названию

            }

            // а сейчас заполняем массив стоимостей
            for (int k = 0; k < count; k++)
            {
                CostPress.Add(int .Parse(mas[i]));  //добавляем стоимость очередного журнала
                i++;    // переходим к следующей стоимости

            }


        }


        /// <summary>
        /// Получает строку, в которой находится один подписчик
        /// </summary>
        /// <param name="index">Порядковый номер подписчика</param>
        /// <returns>Информация о подписчике, подготовленная для таблицы</returns>
        public string StringForVivod(int index)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("| {0,2:d} |", index);
            sb.AppendFormat(" {0,-15}|", this.FIO);
            sb.AppendFormat("{0,7} |", this.Vozrast);

            StringBuilder temp = new StringBuilder();   // временная строка
            temp.Append(NamePress[0]);  //добавляем первый журнал

            // добавляем последующие журналы
            int i = 1;

            while (i < NamePress.Count)
            {
                temp.Append(", ");
                temp.Append(NamePress[i]);
                i++;
            }

            sb.AppendFormat(" {0,-28}|", temp.ToString());
            sb.AppendFormat("{0,10:d} |", this.TotalCost);

            return sb.ToString();

        }

        /// <summary>
        /// обработчик события нажатия на кнопку Home
        /// </summary>
        public void OnHome()
        {
            for (int j = 0; j < this.CostPress.Count; j++)
            {
                CostPress[j] = Convert.ToInt32((int)CostPress[j] * 1.15);
            }
        }

        /// <summary>
        /// обработчик события нажатия на кнопку End
        /// </summary>
        public void OnEnd()
        {
            for (int j = 0; j < CostPress.Count; j++)
            {
                if ((string)NamePress[j] != "Сушка")  // на сушку не распространяется
                {
                    CostPress[j] = Convert.ToInt32((int)CostPress[j] / 1.12);
                }
            }
        }

        /// <summary>
        /// обработчик события нажатия на кнопку Insert
        /// </summary>
        public void OnInsert()
        {
            if (Vozrast < 14)   //проверяем возраст
            {
                bool isNotFind = true;
                for (int j = 0; j < NamePress.Count; j++)
                {
                    if ((string)NamePress[j] == "Сушка")    // если уже есть журнал сушка, то не добавляем
                    {
                        isNotFind = false;
                        break;
                    }
                }
                // если сюда попали, то значит можно  добавить журнал
                if (isNotFind)
                {
                    NamePress.Add("Сушка");
                    CostPress.Add(0); // бесплатный журнал
                }
            }
        }


    }
}
