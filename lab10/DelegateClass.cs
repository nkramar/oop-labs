﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace lab10
{
    
    // делегат который будет обрабатывать нажатия клавиш
    public delegate void key_delegat();
    
    class DelegateClass
    {

        /// <summary>
        /// размер массива делегатов
        /// </summary>
        static int n;

        // поле массив делегатов
        static key_delegat[] MasDelegat;

        /// <summary>
        /// Свойство через которое задаётся размер массива,
        /// а также создаётся сам массив нужного размера
        /// </summary>
        public    static   int   N
        { 
            set  
            { 
                n = value;
                MasDelegat = new   key_delegat[n];
            } 
        }

        /// <summary>
        /// событие, для слежения за нажатием кнопки
        /// </summary>
        public static event key_delegat ClickButtonEvent
        {
            add
            {
                for (int i = 0; i < MasDelegat.Length; i++)
                {
                    if (MasDelegat[i] == null)
                    { 
                        MasDelegat[i] = value;
                        break; 
                    }
                }
            }
            remove
            {
                for (int i = 0; i < MasDelegat.Length; i++)
                {
                    if (MasDelegat[i] == value)
                    {
                        MasDelegat[i] = null;
                        break;
                    }
                }
            }
        } // конец описания события 

        /// <summary>
        /// Метод связанный с событием
        /// </summary>
        /// <param name="k">Нажатая кнопка</param>
        public   static   void   OnPressKey(ConsoleKey   k) 
        {
            for (int i = 0; i < MasDelegat.Length; i++) 
            { 
                if (("On" +k.ToString()) ==  MasDelegat[i].Method.Name)  
                        MasDelegat[i]( ); 
            } 
         } 
    }
}
