﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication3
{
    public partial class Form1 : Form
    {
        ColorDialog colorDialog1 = new ColorDialog();
        public Form1()
        {
            InitializeComponent();
        }

        // событие когда отображается панель
        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            // создаём карандаш чёрный, 1 пиксель шириной
            Pen blackPEN = new Pen(Color.Black, 1);

            // холст для рисования, берём его у панели
            Graphics graphis = panel1.CreateGraphics();
            
            // окончания линии - стрелочка
            blackPEN.EndCap = LineCap.ArrowAnchor;
            // рисуем линию, карандашом
            graphis.DrawLine(blackPEN, 0, 175, 350, 175);

            blackPEN.EndCap = LineCap.NoAnchor;
            blackPEN.StartCap = LineCap.ArrowAnchor;
            graphis.DrawLine(blackPEN, 175, 0, 175, 350);
            blackPEN.StartCap = LineCap.NoAnchor;
            blackPEN.EndCap = LineCap.DiamondAnchor;
            //Укажем на графике единичный отрезок. Он будет равен 7
            graphis.DrawLine(blackPEN, 0, 175, 182, 175);
            graphis.DrawLine(blackPEN, 175, 0, 175, 168);
        }

        //Формула 1, свойство изменения состояния
        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            // Если мы выбрали формулу 1
            if (radioButton1.Checked)
            {
                radioButton1.Checked = false;   //снимаем выделение из формулы 1 (радио 1)

                int start_x;
                int finish_x;

                try
                {
                    // пытаемся преобразовать диапазон
                    start_x = Convert.ToInt32(textBox1.Text);
                    finish_x = Convert.ToInt32(textBox2.Text);
                }
                catch
                {
                    MessageBox.Show("Укажите значения x");
                    return;
                }
                    // отображаем диалоговое окно выбора цвета
                    if (colorDialog1.ShowDialog() == DialogResult.OK)
                    {
                        //создаём карандаш с цветом выбранным в диалогое. Шишина 1 пиксель
                        Pen pen1 = new Pen(colorDialog1.Color, 1);

                        // В зависимости от выбранной толщины
                        switch (comboBox1.SelectedIndex)
                        {
                            case 0:
                                pen1.Width = 1;
                                break;
                            case 1:
                                {
                                    // Выбираем параметры пунктира
                                    float[] line_size = { 2, 3 };
                                    // задаём нашему карандашу чтобы он рисовал пунктиром
                                    pen1.DashPattern = line_size;
                                    pen1.Width = 1;
                                }
                                break;
                            case 2:
                                pen1.Width = 2;
                                break;
                        }

                        // Получаем холс у панели
                        Graphics graphis = panel1.CreateGraphics();

                        //Строим график с учетом того, что единица равна 7

                        // изменяем начало координат и переносим его в точку 175,175
                        graphis.TranslateTransform(175, 175);

                        // начальная точка
                        double prev_x = 7 * start_x;
                        double prev_y = -7 * (start_x / (14 * Math.Sin(start_x*start_x+1)));

                        // берём по очереди точки из диапазона и рассчитываем новые точки
                        for (int x = start_x + 1; x < finish_x; ++x)
                        {
                            try
                            {
                                double now_x = 7 * x;
                                double now_y = -7 * (now_x / (14 * Math.Sin(now_x*now_x+1)));

                                // рисуем линию
                                graphis.DrawLine(pen1, Convert.ToInt32(prev_x), Convert.ToInt32(prev_y),
                                    Convert.ToInt32(now_x), Convert.ToInt32(now_y));

                                // считаем, что новая точка уже устрарела и для следующего расчёта она
                                // будет считаться предыдущей
                                prev_x = now_x;
                                prev_y = now_y;
                            }
                            catch { }
                        }
                    }
            }
        }        

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked)//Что-либо будем делать только, если выбран этот пункт
            {
                radioButton2.Checked = false;
                try
                {
                    int start_x = Convert.ToInt32(textBox1.Text);
                    int finish_x = Convert.ToInt32(textBox2.Text);
                    if (colorDialog1.ShowDialog() == DialogResult.OK)
                    {
                        Pen line = new Pen(colorDialog1.Color, 1);
                        switch (comboBox2.SelectedIndex)
                        {
                            case 0:
                                line = new Pen(colorDialog1.Color, 1);
                                break;
                            case 1:
                                {
                                    float[] line_size = { 2, 3 };
                                    line = new Pen(colorDialog1.Color, 1);
                                    line.DashPattern = line_size;
                                }
                                break;
                            case 2:
                                line = new Pen(colorDialog1.Color, 2);
                                break;
                        }
                        Graphics graphis = panel1.CreateGraphics();
                        graphis.TranslateTransform(175, 175);
                        //Строим график с учетом того, что единица равна 7
                        double prev_x = 7 * start_x;
                        double prev_y = -7 * 3 * Math.Cos(prev_x);
                        for (int x = start_x + 1; x < finish_x; ++x)
                        {
                            double now_x = 7 * x;
                            double now_y = -7 * 3 * Math.Cos(now_x);
                            graphis.DrawLine(line, Convert.ToInt32(prev_x), Convert.ToInt32(prev_y), Convert.ToInt32(now_x), Convert.ToInt32(now_y));
                            prev_x = now_x;
                            prev_y = now_y;
                        }
                    }
                }
                catch
                {
                    MessageBox.Show("Укажите значения x");
                }
            }
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton3.Checked)
            {
                radioButton3.Checked = false;
                try
                {                    
                    int start_x = Convert.ToInt32(textBox1.Text);
                    int finish_x = Convert.ToInt32(textBox2.Text);
                    if (colorDialog1.ShowDialog() == DialogResult.OK)
                    {                        
                        Pen line = new Pen(colorDialog1.Color, 1);
                        switch (comboBox3.SelectedIndex)
                        {
                            case 0:
                                line = new Pen(colorDialog1.Color, 1);
                                break;
                            case 1:
                                {
                                    float[] line_size = { 2, 3 };
                                    line = new Pen(colorDialog1.Color, 1);
                                    line.DashPattern = line_size;
                                }
                                break;
                            case 2:
                                line = new Pen(colorDialog1.Color, 2);
                                break;
                        }
                        Graphics graphis = panel1.CreateGraphics();
                        graphis.TranslateTransform(175, 175);
                        //Строим график с учетом того, что единица равна 7
                        double prev_x = 7 * start_x;
                        double prev_y = -7 * (3 * (Math.Sin(prev_x)));
                        for (int x = start_x + 1; x < finish_x; ++x)
                        {
                            try
                            {
                                double now_x = 7 * x;
                                double now_y = - 7 * (3 * Math.Sin(now_x));
                                graphis.DrawLine(line, Convert.ToInt32(prev_x), Convert.ToInt32(prev_y), Convert.ToInt32(now_x), Convert.ToInt32(now_y));
                                prev_x = now_x;
                                prev_y = now_y;
                            }
                            catch { }
                        }
                    }
                }
                catch
                {
                    MessageBox.Show("Укажите значения x");
                }
            }
        }

        // Кнопка очистить
        private void button1_Click(object sender, EventArgs e)
        {
            panel1.Invalidate();
            textBox1.Clear();
            textBox2.Clear();
        }

      
        
       
    }
}
