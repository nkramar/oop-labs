﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace lab1oop
{
    public enum Diagnosis
    {
        Грипп, Ангина, Пневмония, Аллергия, Инсульт, Инфаркт, Желтуха, Лихорадка, Авитаминоз
    }
    public struct Patient : IComparable
    {
        string name;
        int birthYear;
        DateTime receiptDate;
        Diagnosis diagnosis;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public int BirthYear
        {
            get { return birthYear; }
            set { birthYear = value; }
        }
        public DateTime ReceiptDate
        {
            get { return receiptDate; }
            set { receiptDate = value; }
        }
        public Diagnosis Diagnosis
        {
            get { return diagnosis; }
            set { diagnosis = value; }
        }

        public int StayLengthToDays
        {
            get { return (DateTime.Now - receiptDate).Days; }
        }
        public int Age()
        {
            return DateTime.Now.Year - birthYear;
        }

        public Patient(string patientAsString)
        {
            char[] separators = { ',', ' ' };
            string[] fields = patientAsString.Split(separators, StringSplitOptions.RemoveEmptyEntries);
            name = fields[0];
            birthYear = Convert.ToInt32(fields[1]);
            receiptDate = Convert.ToDateTime(fields[2]);
            diagnosis = (Diagnosis)Enum.Parse(typeof(Diagnosis), fields[3]);
        }

        public int CompareTo(object obj)
        {
            Patient opponent = (Patient)obj;
            if (opponent.StayLengthToDays > StayLengthToDays)
                return -1;
            if (opponent.StayLengthToDays < StayLengthToDays)
                return 1;
            return 0;
        }
    }
}
