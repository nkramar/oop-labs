﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace WindowsFormsApplication2
{
    public partial class Form1 : Form
    {
        Array array;

        public Form1()
        {
            InitializeComponent();
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void обАвтореToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new Form2().Show();
        }

        /// <summary>
        /// выбор в главном меню "Выход"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void новыйToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // отображаем в главном меню "Обработка массива и команду сохранить"
            сохранитьToolStripMenuItem.Enabled = true;
            обработкаМассиваToolStripMenuItem.Enabled = true;

            // отображаем окошко и кнопку для ввода количества элементов массива
            label1.Visible = true;
            textBox1.Visible = true;
            button1.Visible = true;

            // активируем панель инструментов
            toolStrip1.Enabled = true;
        }

        // кнопка Создать массив
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                // создаём массив с количеством элементов, которой введёт пользователь
                int array_length = Convert.ToInt32(textBox1.Text);
                array = new Array(array_length);

                label2.Visible = true;
                // отображаем Сеточку
                dataGridView1.Enabled = true;
                //количество строк у сеточки на одну больше чем элементов массива
                dataGridView1.RowCount = array_length + 1;
                // колонок = 1
                dataGridView1.ColumnCount = 1;      
                // запрещаем пользователю добалять новые элементы
                dataGridView1.AllowUserToAddRows = false;

                // отображаем кнопку ГОТОВО
                button2.Visible = true;
            }
            catch { MessageBox.Show("Нельзя создать такой массив!"); }
        }

        // выбор меню открыть
        private void открытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                // отображаем в главном меню "Обработка массива и команду сохранить"
                сохранитьToolStripMenuItem.Enabled = true;
                обработкаМассиваToolStripMenuItem.Enabled = true;

                // активируем панель инструментов
                toolStrip1.Enabled = true;

                // создаём деалоговое окно открытия файла
                OpenFileDialog openFileDiaolg1 = new OpenFileDialog();

                // указываем диалоговому окну, что нужно отображать только файлы с расширением *.txt
                openFileDiaolg1.Filter = "Text file (*.txt)|*.txt";

                // указываю, чтобы диалоговое окно начинало показывать с папки из которой запущена программа
                openFileDiaolg1.InitialDirectory = Environment.CurrentDirectory;   

                // Отображаем диалоговое окно, и если пользователь там нажмёт ОК, то
                if (openFileDiaolg1.ShowDialog() == DialogResult.OK)
                {
                    // Создаём и открываем файл
                    StreamReader file = new StreamReader(openFileDiaolg1.FileName);

                    // Читаем всё содержимое файла до конца в одну строку
                    string content = file.ReadToEnd();

                    // закрываем файл
                    file.Close();

                    // создаём массив, используя конструктор которому передаётся строка
                    array = new Array(content);
                }
                // отображаем Сеточку
                dataGridView1.Enabled = true;
                dataGridView1.RowCount = array.Length;
                dataGridView1.ColumnCount = 1;
                button2.Text = "Изменить массив";
                button2.Visible = true;

                // копируем элементы из массива array в Сеточку
                for (int index = 0; index < array.Length; ++index)
                    dataGridView1[0, index].Value = array[index];
            }
            catch { }
        }

        private void суммаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int sum = array.Sum();
            if (sum != -1)
                MessageBox.Show("Сумма модулей элементов массива, расположенных после первого нуля, равна " + sum);
            else
                MessageBox.Show("В массиве нет нулей, или после первого нуля нет других элементов!");
        }

        private void отрицательныхЭлементовToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Число отрицательных элементов равно " + array.Amount());
        }

        private void элементовВЗаданномДиапазонеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // активизируем поле, где вводится приделы для счёта
            groupBox1.Enabled = true;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            суммаToolStripMenuItem_Click(sender, e);
        }

        // кнопка Готово
        private void button2_Click(object sender, EventArgs e)
        {
            label1.Visible = false;
            button1.Visible = false;
            textBox1.Visible = false;

            // копируем из сеточки в массив array все элементы
            for (int index = 0; index < array.Length; ++index)
            {
                try
                {
                    array[index] = Convert.ToInt32(dataGridView1[0, index].Value);
                }
                catch { }
            } 
           
            button2.Text = "Изменить массив";
            label2.Visible = false;
            richTextBox1.Visible = false;
            richTextBox2.Visible = false;
            
            // устанавливаем в сетке столькоже элементов, сколько и в массиве.
            dataGridView1.RowCount = array.Length;  

            dataGridView1.ColumnCount = 1;

            // копируем из массива, в сетку элементы
            for (int index = 0; index < array.Length; ++index)
                dataGridView1[0, index].Value = array[index];
        }

        private void поВозрастаниюToolStripMenuItem1_Click(object sender, EventArgs e)
        {                      
            richTextBox1.Visible = true;
            richTextBox1.ReadOnly = true;

            richTextBox2.Visible = true;
            richTextBox2.ReadOnly = true;


            string[] lines = new string[2];

            // получаем две строки, которые нужно вывести в RICH
            lines[0] = "Исходный массив:";
            lines[1]=array.Save();
            richTextBox1.Lines = lines;

            array.Sort_increase();

            lines[0] = "Отсортированный массив:";
            lines[1] = array.Save();
            richTextBox2.Lines = lines;

            button3.Visible = true;
        }

        private void поУбываниюToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            richTextBox1.Visible = true;
            richTextBox2.Visible = true;
            richTextBox1.ReadOnly = true;
            string[] lines = new string[2];
            lines[0] = "Исходный массив:";
            lines[1] = array.Save();
            richTextBox1.Lines = lines;
            array.Sort_decrease();
            richTextBox2.ReadOnly = true;
            lines[0] = "Отсортированный массив:";
            lines[1] = array.Save();
            richTextBox2.Lines = lines;
            button3.Visible = true;
        }

        // кнопка, после нажатия на которую прячутся RICH, а отображается сетка
        private void button3_Click(object sender, EventArgs e)
        {
            richTextBox1.Visible = false;
            richTextBox2.Visible = false;

            button3.Visible = false;
            dataGridView1.RowCount = array.Length;
            dataGridView1.ColumnCount = 1;

            for (int index = 0; index < array.Length; ++index)
                dataGridView1[0, index].Value = array[index];
        }

        private void отрицательныхЭлементовToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Число отрицательных элементов равно " + array.Amount());
        }

        private void сохранитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();

                // указываю, чтобы диалоговое окно начинало показывать с папки из которой запущена программа
                saveFileDialog1.InitialDirectory = Environment.CurrentDirectory;    
                
                saveFileDialog1.Filter = "Text file (*.txt)|*.txt";
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    // открываем файл для записи
                    StreamWriter file = new StreamWriter(saveFileDialog1.FileName);
                    // записываем в файл
                    file.WriteLine(array.Save());

                    // закрываем файл
                    file.Close();
                }
            }
            catch { }
        }

        // диапазоны
        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                // получаем диапазоны
                int limit1 = Convert.ToInt32(textBox2.Text);
                int limit2 = Convert.ToInt32(textBox3.Text);


                // считаем с учётом диапазонов и выводим сразу же результат
                MessageBox.Show("Количество элементов в диапазоне от " 
                    + ((limit1 > limit2) ? limit2 : limit1) + " до " 
                    + ((limit1 < limit2) ? limit2 : limit1) + " равно " 
                    + array.Amount(limit1, limit2));


                // и дезактивизируем группу с диапазонами.
                groupBox1.Enabled = false;
            }
            catch
            {
                MessageBox.Show("Неверно указан интервал!");
            }
        }

        private void поВозрастаниюToolStripMenuItem_Click(object sender, EventArgs e)
        {
            поВозрастаниюToolStripMenuItem1_Click(sender, e);
        }

        private void поУбываниюToolStripMenuItem_Click(object sender, EventArgs e)
        {
            поУбываниюToolStripMenuItem1_Click(sender,e);
        }

        private void вЗаданномДиапазонеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            groupBox1.Enabled = true;
        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        



    }
}

        
        
        