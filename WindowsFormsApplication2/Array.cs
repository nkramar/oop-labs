﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsFormsApplication2
{
    /// <summary>
    /// Массив целых чисел
    /// </summary>
    class Array
    {
        int[] array;        

        /// <summary>
        /// Создание нового массива, ввод данных вручную
        /// </summary>
        /// <param name="array_length">длина массива</param>
        public Array(int array_length)
        {
            array=new int[array_length];
        }


        /// <summary>
        /// Создание нового массива, чтение из файла
        /// </summary>
        /// <param name="file_content">данные из файла</param>
        public Array(string file_content)
        {
            //разделяем строку (file_content) на массив строк, кроме этого отбрасываем все пустые строки
            string[] content = file_content.Split(new char[] { ',', ';', '\n', ' ', '\t', '.' }, StringSplitOptions.RemoveEmptyEntries);
            array = new int[content.Length];    // создаём массив целых чисел, количество которых совпадает с количеством элементов массива строк
            int index;      // индекс для прохода по массиву строк
            int array_index; //индекс для прохода по массиву целых чисел

            // цикл для копирования с преобразованием чисел из массива строк в массив целых
            for (index = 0, array_index = 0; index < content.Length; ++index)
            {
                try
                {
                    array[array_index] = Convert.ToInt32(content[index].Trim());
                    ++array_index;
                }
                catch { }
            }


            if (array_index < index)//Если не все элементы из файла попали в массив...
            {
                int[] temp = new int[array_index];//создадим еще один массив, размерность - число эл-тов, попавших в исходный массив
                for (index = 0; index < array_index; ++index)
                    temp[index] = array[index];//копируем в него эл-ты

                array = temp;   // указываем, чтобы переменная array ссылалась на только что созданный массив.

               
            }


        }

        
        /// <summary>
        /// Длина массива
        /// </summary>
        public int Length
        {          
            get { return array.Length; }
        }
        
        /// <summary>
        /// ИНДЕКСАТОР - Доступ к элементу массива
        /// </summary>
        /// <param name="index">индекс элемента массива</param>
        /// <returns></returns>
        public int this[int index]
        {
            get { return array[index]; }
            set { array[index] = value; }
        }

        /// <summary>
        /// Нахождение числа отрицательных элементов в массиве
        /// </summary>
        /// <returns>Количество отрицательных элементов</returns>
        public int Amount()
        {
            int amount = 0;
            for (int index = 0; index < array.Length; ++index)
                if (array[index] < 0)
                    amount += 1;
            return amount;
        }

        /// <summary>
        /// Количество элементов, лежащих в заданном интервале
        /// </summary>
        /// <param name="limit1">первая граница диапазона</param>
        /// <param name="limit2">вторая граница диапазона</param>
        /// <returns>Число элементов, лежащих в заданном диапазоне</returns>
        public int Amount(int limit1, int limit2)
        {
            int amount = 0, left_limit, right_limit;
            left_limit=(limit1<limit2)?limit1:limit2;//Левая граница диапазона
            right_limit=(limit1>limit2)?limit1:limit2;//Правая граница
            for (int index = 0; index < array.Length; ++index)
                if (array[index] > left_limit && array[index] < right_limit)
                    amount += 1;
            return amount;
        }

        /// <summary>
        /// Сумма модулей эл-тов массива, расположенных после первого нуля
        /// </summary>
        /// <returns></returns>
        public int Sum()
        {
            int index = 0;
            
            // находим индекс нулевого элемента 
            while (index < array.Length && array[index] != 0)
                ++index;
            
            if (index < array.Length-1)     //если индекс левее последнего элемента
            {
                int sum = 0;
                while (index < array.Length)
                {
                    sum += Math.Abs(array[index]);
                    ++index;
                }
                return sum;
            }
            else
                return -1;
        }

        /// <summary>
        /// Сортировка по возрастанию
        /// </summary>
        public void Sort_increase()
        {
            Change();
            System.Array.Sort(array);
        }

        /// <summary>
        /// Сортировка по убыванию
        /// </summary>
        public void Sort_decrease()
        {
            Change();
            System.Array.Sort(array);
            System.Array.Reverse(array);
        }

        /// <summary>
        /// Замена отрицательных элементов квадратами
        /// </summary>
        void Change()
        {
            for (int index = 0; index < array.Length; ++index)
                if (array[index] < 0)
                    array[index] = array[index] * array[index];
        }

        /// <summary>
        /// Функция преобразует весь массив в строку, в которой элементы разделены символом ;
        /// </summary>
        /// <returns>строку с элементами массива</returns>
        public string Save()
        {
            string array_content="";
            for (int index = 0; index < array.Length; ++index)
                array_content += array[index] + "; ";
            return array_content;
        }
    }
}
